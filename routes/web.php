<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\GroupController;
use App\Http\Controllers\QuestionController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [AuthController::class, 'home'])->name('home');

// -- AUTHENTIFICATION --
Route::get('/login', [AuthController::class, 'showLoginForm'])->name('loginForm');
Route::post('/login', [AuthController::class, 'login'])->name('login');
Route::get('/register', [AuthController::class, 'showRegisterForm'])->name('registerForm');
Route::post('/register', [AuthController::class, 'register'])->name('register');


Route::middleware('auth')->group(function () {
    Route::get('/logout', [AuthController::class, 'logout'])->name('logout');

    // -- USER --                                                 NUR ADMIN
    Route::resource('/user', UserController::class)->middleware('can:isAdmin');

    // -- GROUPS --
    Route::resource('/group', GroupController::class);
    Route::post('/addgroup/{group_id}', [GroupController::class, 'addGroup'])->name('addGroup');
    Route::post('/group_leave/{group_id}', [GroupController::class, 'groupLeave'])->name('group_leave');
    Route::post('/group_update_admin/{group_id}/{admin_id}', [GroupController::class, 'groupUpdateAdmin'])->name('group_update_admin');
    Route::post('/group_delete_user/{group_id}/{user_id}', [GroupController::class, 'groupDeleteUser'])->name('group_delete_user');

    // -- QUESTIONS --
    Route::resource('/question', QuestionController::class);
    Route::get('/groupquestions/{group_id}', [QuestionController::class, 'showGroupQuestions'])->name('showGroupQuestions');
    Route::post('/question/{id}/update_level', [QuestionController::class, 'update_level']);

    // -- GAME --
    Route::view('/question/game/{group_id}', 'question.game')->name('question.game');
    Route::post('/game_start/{group_id}', [QuestionController::class, 'gameStart'])->name('gameStart');
    Route::post('/game/{id}/update_level', [QuestionController::class, 'game_update_level'])->name('game_update_level');
    Route::post('/group/{group_id}/reset_level', [QuestionController::class, 'reset_level'])->name('reset_level');
    Route::get('/game_end/{group_id}', [QuestionController::class, 'game_end'])->name('game_end');

    // -- COMMENTS --
    Route::resource('/comment', CommentController::class);
});
