@extends('layout.layout')
@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center align-items-center">
            <div class="col-1"></div>
                <div class="col-md-2">
                    <img src="{{ asset('img/cheering.gif') }}" class="img" alt="...">
                </div>
                <div class="col-6 justify-content-center">
                    <h1 class="display-2 text-center mb-5" style="color: var(--walnut-shell)">Du hast alles geschafft!</h1>
                    <div class="d-flex justify-content-center">
                        <form action="{{ route('home') }}" method="get">
                            @csrf
                            <button class="btn bg-gradient p-2" style="background-color: var(--olive-grove); color: var(--warm-beige);" type="submit">Zurück zu Ihren Gruppen</button>
                        </form>
                    </div>
                </div>
                <div class="col-md-2">
                    <img src="{{ asset('img/cheering.gif') }}" class="img" alt="...">
                </div>
            <div class="col-1"></div>
        </div>
    </div>
@endsection
