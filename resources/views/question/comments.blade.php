@extends('layout.layout')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-1">
                <img src="{{ asset('img/msg.gif') }}" class="img position-fixed" alt="..." id="msg">
            </div>
            <div class="col-md-8 ">
                <div class="card mb-3">
                    <div class="card-body rounded" style="background-color: var(--soft-moss); color: var(--walnut-shell)">
                        <h5 class="card-title text-center mb-3">{{ $question->question }}</h5>
                        <p class="card-text text-center">{{ $question->answer }}</p>
                    </div>
                </div>

                <h5 class="display-4" style="color: var(--walnut-shell)">Kommentare</h5>

                <form action="{{route('comment.store')}}" method="post">
                    @csrf
                    <input type="hidden" name="question_id" value="{{$question->id}}">
                    <div class="input-group mb-3">
                        <input type="text" name="comment" value="{{old('comment')}}" class="form-control my-4" id="new-comment" placeholder="Neuer Kommentar" style="background-color: var(--misty-morning); color: var(--walnut-shell);">
                        @error('comment')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                        <button class="btn bg-gradient my-4" id="check_btn" style="background-color: var(--warm-beige); width: 5rem;" type="submit"><i class="fa-solid fa-check"></i></button>
                    </div>
                </form>
                {{--        All comments          --}}
                @foreach($comments as $comment)
                    <div class="card mb-3">
                        <div class="card-body bg-gradient" style="background-color: var(--warm-beige); color: var(--rich-soil);">
                            <h6 class="card-title"> <small style="color: var(--rich-soil); font-weight: bold">{{ $comment->name }}</small> <small style="color: var(--walnut-shell);">{{ $comment->created_at }}</small></h6>
                            <p class="card-text rounded py-1 px-3" style="width:42rem; background-color: var(--misty-morning); color: var(--walnut-shell);">{{ $comment->text }}</p>
                            @if($question->user_id == Auth::id() || $comment->user_id == Auth::id() || $group_admin_id[0]->admin_id == Auth::id())
                                <div class="container-fluid d-inline-flex justify-content-end p-0">
                                    <form action="{{route('comment.destroy', [$comment->id])}}" method="post">
                                        @csrf
                                        @method('delete')
                                        <button class="btn bg-gradient py-2" style="background-color: var(--walnut-shell); color: var(--warm-beige);" type="submit">Löschen</button>
                                    </form>
                                </div>
                            @endif
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="col-md-1">
                <img src="{{ asset('img/work.gif') }}" class="img position-fixed" alt="..." id="work">
            </div>
        </div>
    </div>
@endsection
