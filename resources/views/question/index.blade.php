@extends('layout.layout')
@section('content')
    <div class="container">

        <div class="row">
            <div class="col-md-2 mx-4">
                <img src="{{ asset('img/thinking.gif') }}" class="img" id="thinking_img" alt="..." >
            </div>
            <div class="col-6 mb-5">
                <div class="card">
                    <div class="card-body rounded" style="background-color: var(--soft-moss);">
                        <form action="{{route('question.store')}}" method="POST">
                            @csrf
                            <div class="mb-3">
                                <label for="question" class="form-label">Frage</label>
                                <input type="text" name="question" value="{{old('question')}}" class="form-control @error('question') is-invalid @enderror" style="color:var(--rich-soil);">
                                @error('question')
                                <div class="invalid-feedback">
                                    {{$message}}
                                </div>
                                @enderror
                            </div>
                            <div>
                                <label for="answer" class="form-label">Antwort</label>
                                <textarea type="text" name="answer" class="form-control @error('answer') is-invalid @enderror" style="height: 10rem; color: var(--rich-soil);"></textarea>
                                @error('answer')
                                <div class="invalid-feedback">
                                    {{$message}}
                                </div>
                                @enderror
                            </div>
                            <div class="container-fluid d-inline-flex justify-content-end my-2">
                                <input type="hidden" name="group_id" value="{{$group_id}}">
                                <button type="submit" class="btn bg-gradient border-0 mt-2" style="background-color: var(--olive-grove); color: var(--warm-beige);">Frage hinzufügen</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="row g-4">
                    @foreach($questions as $question)
                        <div class="col-sm-3">
                            <div class="flip-card">
                                <div class="flip-card-inner">
                                    <div class="flip-card-front">
                                        <div class="card-body">
                                            {{--to go to commentars--}}

                                            <form action="{{route('comment.show', [$question->id])}}" method="get">
                                                @csrf
                                                <button type="submit" class="btn position-absolute top-0 end-0" id="chat_btn"><i class="fa-regular fa-comment"></i></button>
                                            </form>

                                            <h5 class="card-title">{{ $question->question }}</h5>
                                        </div>
                                    </div>
                                    <div class="flip-card-back">
                                        <div class="card-body" style="height: 10rem;">

                                            <p class="card-title mb-2" style="font-size: 1.2rem; height: 50%; overflow: scroll">{{ $question->answer }}</p>
                                            <p>Level: {{ $question->level }}</p>

                                            <div class="col d-inline-flex justify-content-center">
                                                <form action="/question/{{ $question->id }}/update_level" method="post">
                                                    @csrf
                                                    <input type="hidden" name="action" value="happy">
                                                    <button type="submit" class="btn"><i class="fa-regular fa-face-smile" style="color: var(--rich-soil);"></i></button>
                                                </form>
                                                <form action="/question/{{ $question->id }}/update_level" method="post">
                                                    @csrf
                                                    <input type="hidden" name="action" value="neutral">
                                                    <button type="submit" class="btn"><i class="fa-regular fa-face-meh" style="color: var(--rich-soil);"></i></button>
                                                </form>
                                                <form action="/question/{{ $question->id }}/update_level" method="post">
                                                    @csrf
                                                    <input type="hidden" name="action" value="sad">
                                                    <button type="submit" class="btn"><i class="fa-regular fa-face-frown" style="color: var(--rich-soil);"></i></button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <script src="{{ asset('js/flip-card.js') }}"></script>
@endsection
