@extends('layout.layout')
@section('content')
    <div class="container">
        <div class="row justify-content-center ">
            <div class="col-md-2">
                <img src="{{ asset('img/bubududu.gif') }}" class="img" alt="...">
            </div>
            <div class="col-1"></div>
            <div class="col-6" id="q-card_div">
                <div class="row" style="height: 50vh; width: 50vw;">
                <h2 class="text-center">{{ $question->question }}</h2>
                <div class="flip-card" style="height: 20rem;">
                    <div class="flip-card-inner" style="height: 20rem;">
                        <div class="flip-card-front" style="height: 20rem;">
                            <div class="card-body p-5 d-flex justify-content-center align-items-center" style="height: 20rem;">
                                <form action="{{route('comment.show', [$question->id])}}" method="get">
                                    @csrf
                                    <button type="submit" class="btn position-absolute top-0 end-0" id="chat_btn"><i class="fa-regular fa-comment"></i></button>
                                </form>
                                <h3 class="card-title">{{ $question->question }}</h3>
                            </div>
                        </div>
                        <div class="flip-card-back" style="height: 20rem;">
                            <div class="card-body p-3" style="height: 20rem;">

                                <p class="card-title mb-2" style="font-size: 1.2rem; height: 60%; overflow: scroll; ">{{ $question->answer }}</p>
                                <p>Level: {{ $question->level }}</p>

                                <div class="col d-inline-flex justify-content-center">
                                    <form action="{{ route('game_update_level', [$question->id]) }}" method="post">
                                        @csrf
                                        <input type="hidden" name="action" value="happy">
                                        <button type="submit" class="btn"><i class="fa-regular fa-face-smile" style="color: var(--rich-soil);"></i></button>
                                    </form>
                                    <form action="{{ route('game_update_level', [$question->id]) }}" method="post">
                                        @csrf
                                        <input type="hidden" name="action" value="neutral">
                                        <button type="submit" class="btn"><i class="fa-regular fa-face-meh" style="color: var(--rich-soil);"></i></button>
                                    </form>
                                    <form action="{{ route('game_update_level', [$question->id]) }}" method="post">
                                        @csrf
                                        <input type="hidden" name="action" value="sad">
                                        <button type="submit" class="btn"><i class="fa-regular fa-face-frown" style="color: var(--rich-soil);"></i></button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </div>
            <div class="col-1"></div>
            <div class="col-md-2">
                <img src="{{ asset('img/peachcat-cat.gif') }}" class="img" alt="...">
            </div>
        </div>
    </div>
    <script src="{{ asset('js/flip-card.js') }}"></script>
@endsection

