@extends('layout.layout')
@section('content')
    <div class="row">
        <div class="col-6 m-5">
            <div class="card border-0">
                <div class="card-header text-center bg-gradient" style="background-color: var(--rich-soil)">
                    <h2 style="color: var(--warm-beige)">Register</h2>
                </div>
                <div class="card-body rounded-bottom" style="background-color: var(--warm-beige)">
                    <form action="{{ route('register') }}" method="post">
                        @csrf
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" class="form-control" name="name" value="{{old('name')}}" style="background-color: var(--misty-morning)">
                            @error('name')
                            <small class="text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" class="form-control" name="email" value="{{old('email')}}" style="background-color: var(--misty-morning)">
                            @error('email')
                            <small class="text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" class="form-control" name="password" style="background-color: var(--misty-morning)">
                            @error('password')
                            <small class="text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="password_confirmation">Password bestätigen</label>
                            <input type="password" class="form-control" name="password_confirmation" style="background-color: var(--misty-morning)">
                            @error('password_confirmation')
                            <small class="text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                        <div class="form-footer d-flex justify-content-end mt-4">
                            <button type="submit" class="btn bg-gradient" style="background-color: var(--olive-grove); color: var(--warm-beige)">Register</button>
                        </div>
                    </form>
                    <form action="" method="GET" class="position-absolute bottom-0 start-2 mb-3">
                        @csrf
                        <button formaction="{{ route('loginForm') }}" class="btn bg-gradient" style="background-color: var(--earthy-brown); color: var(--warm-beige);">Zurück</button>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-2">
            <img src="{{ asset('img/waiting.gif') }}" class="img" alt="...">
        </div>
    </div>
@endsection
