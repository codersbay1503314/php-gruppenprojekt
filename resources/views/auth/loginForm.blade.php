@extends('layout.layout')
@section('content')
    <div class="row justify-content-center">
        <div class="col-md-4">
            <div class="card border-0">
                <div class="card-header text-center bg-gradient" style="background-color: var(--rich-soil)">
                    <h2 style="color: var(--warm-beige)">Login</h2>
                </div>
                <div class="card-body rounded-bottom" style="background-color: var(--warm-beige)">
                    <form action="{{ route('login') }}" method="post">
                        @csrf
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" class="form-control" name="email" required style="background-color: var(--misty-morning)">
                            @error('email')
                            <small class="text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" class="form-control" name="password" required style="background-color: var(--misty-morning)">
                            @error('password')
                            <small class="text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                        <div class="form-footer d-flex justify-content-between mt-4">
                            <a class="btn bg-gradient" href="{{route('registerForm')}}" style="background-color: var(--earthy-brown); color: var(--warm-beige)">Register</a>
                            <button type="submit" class="btn px-4 bg-gradient" style="background-color: var(--olive-grove); color: var(--warm-beige)">Login</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
        <div class="col-md-2">
            <img src="{{ asset('img/ready.gif') }}" class="img" alt="...">
        </div>
    </div>
@endsection

