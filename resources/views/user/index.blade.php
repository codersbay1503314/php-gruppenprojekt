@extends('layout.layout')
@section('content')
    <div class="container-fluid d-inline-flex justify-content-between">
        <h1 class="display-3 text-center mb-5" style="color: var(--walnut-shell)">Die Liste von Benutzers</h1>
        <div class="col-md-2">
            <img src="{{ asset('img/cheering.gif') }}" class="img mx-3" alt="..." style="font-size: 3rem;">
        </div>
    </div>
    <div class="container-fluid d-flex justify-content-center">
        <div class="card col-md-12">
            <div class="card-body" style="background-color: var(--soft-moss);">
                <table class="table table-hover">
                    <thead>
                    <tr class="table-borderless">
                        <th class="col bg-gradient" style="color: var(--warm-beige); background-color: var(--rich-soil); font-size: 2rem;">ID</th>
                        <th class="col bg-gradient" style="color: var(--warm-beige); background-color: var(--rich-soil); font-size: 2rem;">Name</th>
                        <th class="col bg-gradient" style="color: var(--warm-beige); background-color: var(--rich-soil); font-size: 2rem;">Email</th>
                        <th class="col-sm bg-gradient" style="color: var(--warm-beige); background-color: var(--rich-soil); font-size: 2rem;">Aktionen</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($users as $user)
                        <tr>
                            <td style="color: var(--rich-soil);">{{ $user->id }}</td>
                            <td style="color: var(--rich-soil)">{{ $user->name }}</td>
                            <td style="color: var(--rich-soil)">{{ $user->email }}</td>
                            <td style="width: 20rem">
                                <div class="btn-group">
                                    <form action="" method="GET">
                                        @csrf
                                        <button formaction="{{ route('user.edit', $user->id) }}" class="btn bg-gradient"><i class="fa-solid fa-pen-to-square"></i></button>
                                    </form>
                                    <form action="" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <button formaction="{{ route('user.destroy', $user->id) }}" class="btn mx-4"><i style="color: salmon" class="fa-regular fa-trash-can"></i></button>
                                    </form>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
