@extends('layout.layout')
@section('content')
    <div class="container-fluid d-inline-flex justify-content-between">
        <h1 class="display-3 text-center mb-5" style="color: var(--walnut-shell)">Benutzer bearbeiten</h1>
        <div class="col-md-2">
            <img src="{{ asset('img/cheering.gif') }}" class="img mx-3" alt="..." style="font-size: 3rem;">
        </div>
    </div>
    <div class="container-fluid d-flex justify-content-center">
        <div class="card col-md-12">
            <div class="card-body" style="background-color: var(--soft-moss);">
                <form action="{{ route('user.update', $user->id) }}" method="POST">
                    @csrf
                    @method('PUT')
                    <table class="table table-hover">
                        <thead>
                        <tr class="table-borderless">
                            <th class="col bg-gradient" style="color: var(--warm-beige); background-color: var(--rich-soil); font-size: 2rem;">ID</th>
                            <th class="col bg-gradient" style="color: var(--warm-beige); background-color: var(--rich-soil); font-size: 2rem;">Name</th>
                            <th class="col-sm bg-gradient" style="color: var(--warm-beige); background-color: var(--rich-soil); font-size: 2rem;">Aktionen</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td style="color: var(--rich-soil);">{{ $user->id }}</td>
                            <td style="color: var(--rich-soil);">
                                <input type="text" name="name" value="{{ $user->name }}" class="form-control">
                            </td>
                            <td style="width: 20rem">
                                <button type="submit" class="btn"><i class="fa-regular fa-circle-check"></i></button>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </form>
            </div>
        </div>
    </div>
@endsection
