<!doctype html>
<html lang="en">
<head>
    @vite(['resources/js/app.js'])
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Learn cards</title>
    <link rel="stylesheet" href="{{asset('fontawesome-free-6.5.2-web\fontawesome-free-6.5.2-web\css\all.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
</head>
<body class="bg-body" id="whole_body py-3">

<nav class="navbar navbar-expand-lg p-2 bg-gradient" style="background-color: var(--rich-soil)">
    <div class="collapse navbar-collapse justify-content-xxl-between">
        <small class="text-center px-3" id="hello">Hi, {{ Auth::user()->name ?? 'Guest' }}</small>
        <ul class="navbar-nav">
            @if(Auth::check())
                <li class="nav-item">
                    <a class="nav-link" style="color: var(--warm-beige)" aria-current="page" href="{{route('home')}}">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" style="color: var(--warm-beige)" href="{{route('group.index')}}">Groups</a>
                </li>
                @can('isAdmin')
                    <li class="nav-item">
                        <a class="nav-link" style="color: var(--warm-beige)" href="{{route('user.index')}}">Users</a>
                    </li>
                @endcan
                <li class="nav-item">
                    <a class="nav-link" style="color: var(--warm-beige)" href="{{route('logout')}}">Logout</a>
                </li>
            @endif
        </ul>
    </div>
</nav>
<div class="container-fluid py-5" id="body">
    @section('content')
    @show
</div>
</body>
<footer class="blockquote-footer position-fixed bottom-0 container-fluid"
        style="background-color: transparent; color: var(--forest-floor)">
    @copyright Dani Ayat Dijana {{ date('Y') }}
</footer>
</html>
