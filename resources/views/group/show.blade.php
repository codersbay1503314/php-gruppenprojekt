@extends('layout.layout')
@section('content')
    <div class="display-2 m-3" style="color: var(--walnut-shell);"> {{$group->name}} </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 mx-3">
                <div class="card">
                    <div class="card-header bg-gradient" style="background-color: var(--rich-soil); color: var(--warm-beige);">
                        <div class="display-2"> Mitglieder </div>
                    </div>
                    <div class="card-body" style="background-color: var(--soft-moss); color: var(--walnut-shell);">
                        <table class="table table-hover">
                            <thead>
                            <tr class="table-borderless">
                                <th scope="col" class="col-11" style="color: var(--walnut-shell);">Name</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach( $users as $user )
                                    <tr>
                                        <td style="color: var(--walnut-shell);">
                                            {{ $user->name }}
                                            @if($user->id == $group->admin_id)
                                                <i class="fa-solid fa-crown mx-1" style="color: var(--honeycomb); font-size: 1rem;"></i>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                            <div class="d-flex justify-content-between">
                                <form action="{{ route('gameStart', [$group->id]) }}" method="post">
                                    @csrf
                                    <input type="submit" value="Starte die Abfrage" class="btn bg-gradient" style="background-color: var(--olive-grove); color: var(--warm-beige);">
                                </form>

                                <form action="{{route('showGroupQuestions', ['group_id' => $group->id])}}" method="get">
                                    @csrf
                                    <input type="submit" value="Fragen" class="btn bg-gradient" style="background-color: var(--olive-grove); color: var(--warm-beige);">
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            <div class="col-md-4 mx-4">
                <img src="{{ asset('img/dancers.gif') }}" class="img" alt="...">
            </div>
        </div>
    </div>
@endsection
