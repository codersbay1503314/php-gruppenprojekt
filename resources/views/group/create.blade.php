@extends('layout.layout')
@section('content')
    <div class="row justify-content-center mx-5">
        <div class="col-md-3">
            <div class="card">
                <div class="card-header text-center bg-gradient"
                     style="background-color: var(--rich-soil); color: var(--warm-beige);">
                    <h3 class="card-title">Neue Gruppe</h3>
                </div>
                <div class="card-body bg-gradient" style="background-color: var(--warm-beige);">
                    <form action="{{route('group.store')}}" method="POST">
                        @csrf
                        <div class="form-group my-3">
                            <label for="name">Gruppenname:</label>
                            <input type="text" class="form-control" name="name"
                                   style="background-color: var(--misty-morning); color: var(--walnut-shell);">
                            @error('name')
                            <small class="text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                        <div class="form-footer d-md-flex justify-content-md-end mt-4">
                            <button type="submit" class="btn bg-gradient"
                                    style="background-color: var(--olive-grove); color: var(--warm-beige);">Erstellen
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-9">
            <div class="row row-cols-1 row-cols-md-2 g-4">
                @foreach($groups as $group)
                    <div class="card p-0 mx-3" style="height: 12rem; width: 16rem; background-color: var(--warm-beige)">
                        <div class="card-header bg-gradient" style="background-color: var(--rich-soil);">
                            <h5 class="card-title" style="color: var(--warm-beige);">{{$group->name}}</h5>
                        </div>
                        <div class="card-body p-0">
                            <div class="row mb-3" style="height: 4.6rem;">
                                <img src="{{ asset('img/cheering.gif') }}" class="img"  id="cheering_img" alt="...">
                            </div>
                            <form  class="container-fluid d-inline-flex justify-content-end mx-0" action="{{route('addGroup', [$group->id])}}" method="POST">
                                @csrf
                                <button type="submit" class="btn bg-gradient" style="background-color: var(--olive-grove); color: var(--honeycomb);">Gruppe beitreten</button>
                            </form>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection
