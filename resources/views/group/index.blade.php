@extends('layout.layout')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="row d-inline-flex justify-content-between">
                <h1 class="col display-1 my-3" style="color: var(--walnut-shell)">Gruppen:</h1>
                <div class="col-md-2">
                    <img src="{{ asset('img/dancers.gif') }}" class="img mx-3" alt="..." id="dancers_img">
                </div>
            </div>
            <div class="row mx-1">
                <a href="{{route('group.create')}}" class="btn bg-gradient my-3 py-2" style="width: 15rem; background-color: var(--olive-grove); color: var(--warm-beige)">Gruppe hinzufügen</a>
            </div>
            <div class="row g-4">
                @foreach($groups as $group)
                    <div class="col-sm-2">
                        <div class="card" style="height: 13rem">
                            <div class="card-header bg-gradient"
                                 style="background-color: var(--rich-soil); color: var(--warm-beige);">
                                <h5 class="card-title">{{$group->name}}</h5>
                            </div>
                            <div class="card-body bg-gradient" style="background-color: var(--warm-beige);">
                                <div class="d-flex justify-content-start my-3">
                                    <form action="{{route('group.show', [$group->id])}}" method="GET">
                                        @csrf
                                        <input type="submit" value="Gruppe anzeigen" class="btn bg-gradient" style="background-color: var(--forest-floor); color: var(--warm-beige); width: 9rem;">
                                    </form>
                                @if($group->admin_id == Auth::id())
                                        <form action="{{ route('group.edit', [$group->id]) }}" method="GET">
                                            @csrf
                                            <button type="submit" class="btn">
                                                <i class="fa-solid fa-pen-to-square"></i>
                                            </button>
                                        </form>
                                    @endif
                                </div>
                                <div class="d-flex justify-content-end">
                                    <form action="{{ route('group_leave', [$group->id]) }}" method="POST">
                                        @csrf
                                        <button type="submit" class="btn">
                                            <i style="color: salmon;" class="fa-solid fa-door-open"></i>
                                        </button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection
