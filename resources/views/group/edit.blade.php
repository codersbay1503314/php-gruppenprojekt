@extends('layout.layout')
@section('content')
    <div class="d-flex justify-content-between p-5">
        <div class="col-6">
            <div class="card">
                <div class="card-header bg-gradient"
                     style="background-color: var(--rich-soil); color: var(--warm-beige);">
                    <div class="display-5 my-2" > {{ $group->name }} </div>
                </div>
                <div class="card-body" style="background-color: var(--soft-moss); color: var(--walnut-shell);">
                    <form action="{{route('group.update', [$group->id])}}" method="POST">
                        @csrf
                        @method('PUT')
                        <div class="form-group p-3 mb-2">
                            <label for="name" class="col-form-label-lg">Gruppenname:</label>
                            <input type="text" name="name" class="form-control" value="{{$group->name}}">
                            @error('name')
                            <small class="text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                        <div class="d-flex justify-content-end">
                            <button type="submit" class="btn bg-gradient mx-3 mb-5" style="background-color: var(--olive-grove); color: var(--warm-beige);">Speichern</button>
                        </div>
                    </form>
                    <div class="container-fluid d-flex justify-content-center my-3">
                        <div class="card col-md-12">
                            <div class="card-body rounded" style="background-color: var(--soft-moss);">
                                <table class="table table-hover table-sm">
                                    <thead>
                                    <tr class="table-borderless bg-gradient">
                                        <th class="col-1" style="color: var(--warm-beige); background-color: var(--rich-soil); ">ID</th>
                                        <th class="col-10" style="color: var(--warm-beige); background-color: var(--rich-soil);">Name</th>
                                        <th class="col-1 " style="color: var(--warm-beige); background-color: var(--rich-soil);">Aktionen</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($users as $user)
                                        <tr>
                                            <td style="color: var(--rich-soil)">{{ $user->id }}</td>
                                            @if($user->id == Auth::id())
                                                <td style="color: var(--rich-soil)">Du</td>
                                                <td style="width: 20rem"></td>
                                            @else
                                                <td style="color: var(--rich-soil)">{{ $user->name }}</td>

                                                <td class="col d-inline-flex" style="width: 6rem;">
                                                    <form action="{{ route('group_update_admin', [$group->id, $user->id]) }}" method="post">
                                                        @csrf
                                                        <button type="submit" class="btn bg-gradient"><i id="crown" class="fa-solid fa-crown"></i></button>
                                                    </form>
                                                    <form action="{{  route('group_delete_user', [$group->id, $user->id])}}" method="POST">
                                                        @csrf
                                                        <button class="btn" type="submit"><i id="trash_icon" class="fa-regular fa-trash-can"></i></button>
                                                    </form>
                                                </td>
                                            @endif
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="container d-inline-flex justify-content-between">
                        <a href="{{route('home')}}" class="btn bg-gradient" style="background-color: var(--walnut-shell); color: var(--warm-beige);">Züruck</a>
                        <form action="{{route('group.destroy', [$group->id])}}" method="post">
                            @csrf
                            @method('delete')
                            <input type="submit" value="Gruppe löschen" class="btn bg-gradient" style="background-color: var(--olive-grove); color: var(--warm-beige);">
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <img src="{{ asset('img/sleepy.gif') }}" class="img" alt="...">
        </div>
    </div>
@endsection
