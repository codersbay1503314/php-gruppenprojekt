const flipCards = document.querySelectorAll('.flip-card');


flipCards.forEach((flipCard) => {
    flipCard.addEventListener('click', (event) => {
        if (event.target.closest('.flip-card') === flipCard) {
            flipCard.classList.toggle('flipped');
        }
    });
});

