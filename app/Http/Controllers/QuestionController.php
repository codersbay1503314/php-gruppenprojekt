<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Foundation\Application;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class QuestionController extends Controller
{
    public function showGroupQuestions(string $group_id)
    {
        $questions = DB::select('SELECT * FROM questions
         JOIN question_level ON questions.id = question_level.question_id
         WHERE group_id = ?
        AND question_level.user_id = ?
        ', [$group_id, Auth::id()]);
        return view('question.index', ['questions' => $questions, 'group_id' => $group_id]);
    }

    //Show the form for creating a new resource.
    public function create(): Factory|Application|View|\Illuminate\Contracts\Foundation\Application
    {
        return view('question.create');
    }

    //Store a newly created resource in storage.
    public function store(Request $request): RedirectResponse
    {

        $validatedData = $request->validate([
            'question' => 'required|max:255',
            'answer' => 'required|max:255',
            'group_id' => 'required'
        ]);

        $group_id = $validatedData['group_id'];

        DB::table('questions')->insert([
            'question' => $validatedData['question'],
            'answer' => $validatedData['answer'],
            'group_id' => $validatedData['group_id'],
            'user_id' => Auth::id()
        ]);

        $question_id = DB::select('SELECT id FROM questions order by id desc limit 1');

        $allGroupUsers = DB::select('SELECT user_id FROM user_in_group WHERE group_id = ?', [$group_id]);

        foreach ($allGroupUsers as $user) {
            DB::table('question_level')->insert([
                'user_id' => $user->user_id,
                'question_id' => $question_id[0]->id,
                'level' => 3
            ]);
        }

        return redirect()->route('showGroupQuestions', ['group_id' => $group_id])->with('success', 'Question Added successfully!.');
    }

    //Show the form for editing the specified resource.
    public function edit(string $id): Factory|Application|View|\Illuminate\Contracts\Foundation\Application
    {
        $question = DB::table('questions')->where('id', $id)->first();
        $questions = DB::select('SELECT * FROM questions');
        return view('question.edit', ['questions' => $questions, 'editquestion' => $question]);
    }

    //Update the specified resource in storage.
    public function update(Request $request, string $id)
    {
        $validatedData = $request->validate([
            'question' => 'required',
            'answer' => 'required',
        ]);

        DB::table('questions')->where('id', $id)->update([
            'question' => $validatedData['question'],
            'answer' => $validatedData['answer'],
        ]);
        return redirect('/question')->with('success', 'Question Updated successfully!.');
    }

    //Remove the specified resource from storage.
    public function destroy(string $id): Application|Redirector|\Illuminate\Contracts\Foundation\Application|RedirectResponse
    {
        DB::table('questions')->where('id', $id)->delete();
        return redirect('/question')->with('success', 'Question deleted successfully!.');
    }


   /*--------- Game funktions ---------*/


// spiel starten
    public function gameStart(string $group_id)
    {
        // get all questions
        $questions = DB::table('questions')
            ->join('question_level', 'questions.id', '=', 'question_level.question_id')
            ->where('group_id', $group_id)
            ->where('question_level.user_id', Auth::id())
            ->get();

        $count_level_one_questions = 0;

        // add question into question pool if the question->level is higher than 1
        foreach ($questions as $question) {
            if ($question->level == 3) {
                $questions[] = $question;
            }
            if ($question->level >= 2) {
                $questions[] = $question;
            }
            if ($question->level == 1) {
                $count_level_one_questions++;
            }
        }

        // end the game if all questions are level 1
        if ($questions->count() <= $count_level_one_questions) {
            return redirect()->route('game_end', [$group_id]);
        }

        // frage an view
        return view('question.game', ['question' => $questions->random(), 'group_id' => $group_id]);
    }

// spiel ende
    public function game_end($group_id)
    {
        $questions_ids = DB::table('question_level')
            ->join('questions', 'questions.id', '=', 'question_level.question_id')
            ->where('group_id', $group_id)
            ->where('question_level.user_id', Auth::id())
            ->get('question_id');

        foreach ($questions_ids as $question_id) {
            DB::table('question_level')
                ->where('user_id', Auth::id())
                ->where('question_id', $question_id->question_id)
                ->update(['level' => 3]);
        }
        return view('question.game_end');

    }

    public function game_update_level(Request $request, $id)
    {
        $question_id = $id;
        $action = $request->input('action');

        // aktuelles Level aus der Datenbank
        $currentLevel = DB::table('question_level')
            ->where('user_id', Auth::id())
            ->where('question_id', $question_id)
            ->value('level');

        if ($action == 'happy') {
            $newLevel = max($currentLevel - 1, 1);
        } elseif ($action == 'sad') {
            $newLevel = min($currentLevel + 1, 3);
        } else {
            $newLevel = $currentLevel;
        }

        DB::table('question_level')
            ->where('user_id', Auth::id())
            ->where('question_id', $question_id)
            ->update(['level' => $newLevel]);

        $group_id = DB::table('questions')->where('id', $question_id)->value('group_id');

        return $this->gameStart($group_id);
    }

    public function update_level(Request $request, $id)
    {
        $question_id = $id;
        $action = $request->input('action');

        // aktuelles Level aus der Datenbank
        $currentLevel = DB::table('question_level')
            ->where('user_id', Auth::id())
            ->where('question_id', $question_id)
            ->value('level');

        if ($action == 'happy') {
            $newLevel = max($currentLevel - 1, 1);
        } elseif ($action == 'sad') {
            $newLevel = min($currentLevel + 1, 3);
        } else {
            $newLevel = $currentLevel;
        }

        DB::table('question_level')
            ->where('user_id', Auth::id())
            ->where('question_id', $question_id)
            ->update(['level' => $newLevel]);

        $group_id = DB::table('questions')->where('id', $question_id)->value('group_id');

        return redirect()->back()->with('success', 'Level updated successfully!.');
    }
}
