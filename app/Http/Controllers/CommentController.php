<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CommentController extends Controller
{
    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'comment' => 'required|max:255',
        ]);

        DB::table('comments')->insert([
            'user_id' => Auth::id(),
            'question_id' => $request->get('question_id'),
            'text' => $request->get('comment'),
        ]);
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $question = DB::table('questions')->where('id', $id)->first();
        $comments = DB::select('select comments.id as id, comments.*, users.id as user_id, users.name from comments join users on users.id = comments.user_id where question_id = ? order by comments.id desc', [$id]);
        $group_admin_id = DB::select('SELECT groups.admin_id FROM groups WHERE groups.id = ?', [$question->group_id]);

        return view('question.comments', ['comments' => $comments, 'question' => $question, 'group_admin_id' => $group_admin_id]);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $comment = DB::table('comments')->where('id', $id)->get();
        $comment_user_id = $comment[0]->user_id;
        $question = DB::table('questions')->where('id', $comment[0]->question_id)->get();
        $question_user_id = $question[0]->user_id;
        $group_id = $question[0]->group_id;
        $group_admin_id = DB::select('SELECT groups.admin_id FROM groups WHERE groups.id = ? ', [$group_id]);
        $group_admin_id = $group_admin_id[0]->admin_id;

        if($question_user_id == Auth::id() || $comment_user_id == Auth::id() || $group_admin_id == Auth::id()){
            DB::table('comments')->where('id', $id)->delete();
        } else {
            return redirect()->route('home');
        }
        return redirect()->back();
    }

}
