<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;

class UserController extends Controller
{
    //Display a listing of the resource.

    public function index()
    {
        if (Auth::check()) {
            if (Gate::allows('isAdmin')) {
                $users = DB::select('SELECT * FROM users');
                return view('user.index', ['users' => $users]); //, 'editProduct' => null]);
            }
        }
        return redirect('/');
    }

    //Store a newly created resource in storage.
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required|string|unique:users|max:255',
        ]);

        DB::table('groups')->insert([
            'name' => $validatedData['name'],
        ]);

        return redirect()->route('user.index')->with('success', 'User created successfully!.');
    }

    // Show the form for editing the specified resource.
    public function edit(string $id)
    {
        if (Auth::check()) {
            if (Gate::allows('isAdmin')) {
                $user = DB::table('users')->where('id', $id)->first();
                $users = DB::select('SELECT * FROM users');

                if ($user) {
                    return view('user.edit', compact('user'));
                }
                return view('index')->with('error', 'User not found!');
            }
        }
        return redirect('/');
    }

    // Update the specified resource in storage.
    public function update(Request $request, string $id)
    {
        if (Auth::check()) {
            if (Gate::allows('isAdmin')) {
                $validatedData = $request->validate([
                    'name' => 'required|max:255'
                ]);

                DB::table('users')->where('id', $id)->update([
                    'name' => $validatedData['name']
                ]);
                return redirect()->route('user.index')->with('success', 'User updated successfully!.');
            }
        }
        return redirect('/');
    }

    //Remove the specified resource from storage.
    public function destroy(string $id)
    {
        if (Auth::check()) {
            if (Gate::allows('isAdmin')) {
                $userIdToDestroy = $id;
                DB::table('question_level')->where('user_id', $userIdToDestroy)->delete();
                $user_in_group_list = DB::table('user_in_group')->where('user_id', $userIdToDestroy)->get();
                foreach ($user_in_group_list as $user_in_group_l) {
                    $group = DB::table('groups')->where('id', $user_in_group_l->group_id)->first();

                    if ($group->admin_id == $userIdToDestroy) {
                        $users_in_group = DB::table('user_in_group')->where('group_id', $group->id)->get();
                        if (sizeof($users_in_group) > 1) {
                            foreach ($users_in_group as $user_in_group) {
                                if ($user_in_group->user_id != $userIdToDestroy) {
                                    DB::table('groups')->where('id', $user_in_group->group_id)->update([
                                        'admin_id' => $user_in_group->user_id,
                                    ]);
                                    break;
                                }
                            }
                        }
                        else {
                            DB::delete('DELETE question_level FROM question_level
                        JOIN questions ON questions.id = question_level.question_id
                        WHERE questions.group_id = ?', [$group->id]);
                            DB::table('groups')->where('id', $group->id)->delete();
                            DB::table('questions')->where('group_id', $group->id)->delete();
                        }
                    }
                }
                DB::table('user_in_group')->where('user_id', $userIdToDestroy)->delete();
                DB::table('users')->where('id', $userIdToDestroy)->delete();

                return redirect('/user')->with('success', 'User deleted successfully!.');
            }
        }
        return redirect('/');
    }
}
