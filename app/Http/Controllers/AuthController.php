<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function home(){
        if(Auth::check()){
            if (Gate::allows('isAdmin')){
                return redirect()->route('user.index');
            } else {
                return redirect()->route('group.index');
            }
        }
        return redirect()->route('loginForm');
    }


    public function showRegisterForm()
    {
        return view('auth.registerForm');
    }

    public function register(Request $request)
    {
        $validateData = $request->validate([
            'name' => 'required|max:255',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|min:4|confirmed',
        ]);

        $user = new User;
        $user->name = $validateData['name'];
        $user->email = $validateData['email'];
        $user->password = Hash::make($validateData['password']);
        $user->role = 'USER';

        $user->save();

        return redirect('/')->with('success', 'Account created successfully!');
    }

    public function showLoginForm()
    {
        return view('auth.loginForm');
    }

    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');

        if(Auth::attempt($credentials)) {
            $request->session()->regenerate();
            return redirect('/')->with('success', 'Logged in successfully');
        }
        return back()->withErrors(['email' => 'False email or password. Please try again.']);
    }

    public function logout(Request $request)
    {
        Auth::logout();

        $request->session()->invalidate();
        $request->session()->regenerateToken();

        return redirect()->route('home');
    }
}
