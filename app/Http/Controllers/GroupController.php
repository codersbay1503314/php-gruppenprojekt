<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class GroupController extends Controller
{
    //Display a listing of the resource.
    public function index()
    {
        $groups = DB::select('SELECT * FROM groups
                                    JOIN user_in_group ON groups.id = user_in_group.group_id
                                    WHERE user_in_group.user_id LIKE ?', [Auth::id()]);

        return view('group.index', ['groups' => $groups]);
    }

    // Show the form for creating a new resource.
    public function create()
    {
        $groups = DB::select('SELECT * FROM groups EXCEPT SELECT groups.* FROM groups JOIN user_in_group ON groups.id = user_in_group.group_id WHERE user_in_group.user_id LIKE ?', [Auth::id()]);

        return view('group.create', ['groups' => $groups]);
    }

    //Store a newly created resource in storage.
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required|string|unique:groups,name|max:255',
        ]);

        DB::table('groups')->insert([
            'name' => $validatedData['name'],
            'admin_id' => Auth::id(),
        ]);
        $newGroup = DB::table('groups')->orderBy('id', 'desc')->first();

        DB::table('user_in_group')->insert([
            'user_id' => Auth::id(),
            'group_id' => $newGroup->id,
        ]);

        return redirect()->route('group.index')->with('success', 'Group created successfully!.');
    }

    public function addGroup(string $group_id)
    {
        DB::table('user_in_group')->insert([
            'user_id' => Auth::id(),
            'group_id' => $group_id,
        ]);

        $questions = DB::select('SELECT id FROM questions where group_id = ?', [$group_id]);

        foreach ($questions as $question) {
            DB::insert('INSERT INTO question_level (user_id, question_id, level) VALUES (?, ?, ?)', [Auth::id(), $question->id, 3]);
        }

        return redirect()->route('showGroupQuestions', ['group_id' => $group_id]);

    }

    //Display the specified resource.
    public function show(string $id)
    {
        $group = DB::table('groups')->where('id', $id)->first();
        $users = DB::table('user_in_group')
            ->join('users', 'users.id', '=', 'user_in_group.user_id')
            ->where('group_id', $id)->get();

        return view('group.show', ['group' => $group, 'users' => $users]);
    }

    // Show the form for editing the specified resource.
    public function edit(string $id)
    {
        $group = DB::table('groups')->where('id', $id)->first();
        $users = DB::table('user_in_group')
            ->join('users', 'users.id', '=', 'user_in_group.user_id')
            ->where('group_id', $id)->get();
        return view('group.edit', ['group' => $group, 'users' => $users]);
    }

    // Update the specified resource in storage.
    public function update(Request $request, string $id)
    {
        $validatedData = $request->validate([
            'name' => 'required|max:255',
        ]);

        DB::table('groups')->where('id', $id)->update([
            'name' => $validatedData['name'],
        ]);
        return redirect()->route('group.index')->with('success', 'Produkt erfolgreich aktualisiert.');
    }

    public function groupUpdateAdmin(int $group_id, int $admin_id){
        DB::table('groups')->where('id', $group_id)->update([
            'admin_id' => $admin_id,
        ]);
        return redirect()->route('home');
    }

    //Remove the specified resource from storage.
    public function destroy(string $id)
    {
        $admin_id = DB::table('groups')->where('id', $id)->get('admin_id');

        if (Auth::id() == $admin_id[0]->admin_id) {
            DB::delete('DELETE question_level FROM question_level
                        JOIN questions ON questions.id = question_level.question_id
                        WHERE questions.group_id = ?', [$id]);
            DB::table('groups')->where('id', $id)->delete();
            DB::table('questions')->where('group_id', $id)->delete();
            DB::table('user_in_group')->where('group_id', $id)->delete();
        }

        return redirect()->route('group.index')->with('success', 'Group deleted successfully!.');
    }

    public function groupLeave(int $id)
    {
        // delete question_levels from this user
        DB::table('question_level')
            ->join('questions', 'questions.id', '=', 'question_level.question_id')
            ->where('questions.group_id', $id)
            ->where('question_level.user_id', Auth::id())
            ->delete();

        // delete user in group
        DB::delete('DELETE FROM user_in_group WHERE group_id = ? AND user_id = ?', [$id, Auth::id()]);

        $group_admin = DB::select('SELECT admin_id FROM groups WHERE id = ?', [$id]);
        $left_users_in_group = DB::select('SELECT user_id FROM user_in_group WHERE group_id = ?', [$id]);

        if ($group_admin[0]->admin_id == Auth::id()) {
            if ($left_users_in_group == null) {
                $this->destroy($id);
            } else {
                DB::table('groups')->where('id', $id)->update([
                    'admin_id' => $left_users_in_group[0]->user_id,
                ]);
            }
        }

        return redirect()->route('group.index');
    }

    public function groupDeleteUser(int $group_id, int $user_id){
        DB::table('user_in_group')->where('user_id', $user_id)->where('group_id', $group_id)->delete();
        $questions = DB::table('questions')->where('group_id', $group_id)->get('id');

        foreach ($questions as $question){
            DB::table('question_level')->where('user_id', $user_id)->where('question_id', $question->id)->delete();
        }
        return redirect()->back();
    }
}
